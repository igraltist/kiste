# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
inherit multilib

DESCRIPTION="Rule Set Based Access Control (RSBAC) Admin Tools"
HOMEPAGE="http://www.rsbac.org/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="initrd"

DEPEND="
	sys-apps/rsbac-admin
	sys-kernel/rsbac-sources"

RDEPEND="${DEPEND}"


pkg_postinst() {
	rsbac_sources="/usr/src/linux"
	if [ -d "${rsbac_sources}/rsbac" ]; then
		# copy modified Kconfig to the kernel source
		if use initrd; then 
			cp "${FILESDIR}"/Kconfig ${rsbac_sources}/rsbac
		else 
			cp "${FILESDIR}"/Kconfig.initrd ${rsbac_sources}/rsbac
		fi

		einfo "Now you can build and install your rsbac patched kernel."

	else
		eerror "No rsbac-sources available."
		eerror "Please do install or reinstall the rsbac-sources first."
		eerror "For example:"
		eerror "emerge -av rsbac-sources"
	fi
}

