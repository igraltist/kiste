# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit multilib toolchain-funcs git-r3 user 

DESCRIPTION="Rule Set Based Access Control (RSBAC) Admin Tools"
HOMEPAGE="http://www.rsbac.org/"

EGIT_REPO_URI="git://rsbac.org/${PN}.git"
EGIT_COMMIT="f6e42463230ab9b5bf8050fa0fde4f95b8be5896"
# Date:   Fri Jul 7 13:03:29 2017 +0200

EPATCH_SUFFIX="patch"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm"
# using rklogd is deprecated but offer the option since it is fully removed
# from source
IUSE="pam rklogd"

DEPEND="
	dev-util/dialog
	pam? ( sys-libs/pam )
	sys-apps/baselayout
	>=sys-libs/ncurses-5.2"

RDEPEND="${DEPEND}"

src_compile() {
	local rsbacmakeargs="libs tools"
	use rklogd && rsbacmakeargs="${rsbacmakeargs} rklogd"
	use pam && rsbacmakeargs="${rsbacmakeargs} pam nss"
	emake PREFIX=/usr LIBDIR=/$(get_libdir) ${rsbacmakeargs}
}

src_install() {
	local rsbacinstallargs="headers-install libs-install tools-install"
	use rklogd && rsbacinstallargs="${rsbacinstallargs} rklogd-install"
	use pam && rsbacinstallargs="${rsbacinstallargs} pam-install nss-install"
	emake PREFIX=/usr LIBDIR=/$(get_libdir) DESTDIR="${D}" ${rsbacinstallargs}

	#FHS compliance
	dodir /usr/$(get_libdir)
	mv "${D}"/$(get_libdir)/librsbac.{,l}a "${D}"/usr/$(get_libdir)
	mv "${D}"/$(get_libdir)/libnss_rsbac.{,l}a "${D}"/usr/$(get_libdir)
	gen_usr_ldscript librsbac.so
	gen_usr_ldscript libnss_rsbac.so

	# copy file to /etc
	use rklogd && doinitd "${FILESDIR}"/rklogd

	dodir /etc/rsbac
	insinto /etc/rsbac/
	doins "${FILESDIR}"/rsbac.conf

	use pam && {
		insinto /etc/pam.d
		doins "${FILESDIR}"/system-auth

		insinto /etc
		doins "${FILESDIR}"/nsswitch.conf

	}
}

pkg_postinst() {
	enewgroup security 400
	enewuser security 400 /bin/bash /home/admins/security security
	
	chown security:security /home/admins/security
	chmod 0750 /home/admins/security

	elog "We suggest you run a separate copy of syslog-ng (for example) to log RSBAC"
	elog "messages as user 'audit' (uid 404) instead of using the deprecated rklogd."
	elog "See"
	elog
	elog "    http://www.rsbac.org/documentation/administration_examples/syslog-ng"
	elog
	elog "for more information."
	elog "********************************************************************************"
}
