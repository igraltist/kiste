# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit user multilib toolchain-funcs

DESCRIPTION="QEMU commandline tools"
HOMEPAGE="https://hg.kasten-edv.de/kvmtools"
SRC_URI="https://hg.kasten-edv.de/kvm-tools/archive/tip.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="vnc"

DEPEND="
	sys-apps/iproute2
	net-misc/bridge-utils
	sys-apps/usermode-utilities
	"

RDEPEND="
	${DEPEND}
	app-emulation/qemu
	"

src_unpack() {

	echo "done"
}

src_install() {
	# create directory for socket- and pidfiles
	dodir /var/run/kvm
	diropts -m0750 
	fowners kvm 

}
