# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
ETYPE="soures"

inherit kernel-2
detect_version
detect_arch

DESCRIPTION="RSBAC kernel sources (kernel series ${KV_MAJOR}.${KV_MINOR})"
HOMEPAGE="https://www.rsbac.org"

EGIT_REPO_URI="git://rsbac.org/${PN}.git"
EGIT_COMMIT="460fa2bf6102e2ae9b56df7c947413908967de3"
# Thu, 15 Jun 2017 11:30:41 +0000 (13:30 +0200)


IUSE=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm"

pkg_postinst() {
	kernel-2_pkg_postinst
}

pkg_postrm() {
	kernel-2_pkg_postrm
}

